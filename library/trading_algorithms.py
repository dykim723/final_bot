import numpy as np
import pandas as pd

from library.logging_pack import *
# type 1 추세 추종
# type 2 반전
def BBands(df, w=20, k=2, type=1):
    """
        w: 이동평균선 기간 값 (20)
        k: 승수 (2)
        std 함수는 '표준편차를 구하기 위한' numpy 패키지에 포함되어 있는 내장 함수입니다.
        DATAFRAME[-1:] 마지막 row
        DATAFRAME[:-1] index 0부터 마지막 row 제외한 rows
        DATAFRAME[-20:] 뒤에서부터 20개의 rows
        DATAFRAME[:20] index 0부터 20개의 rows
        DATAFRAME[20:] index 20부터 끝까지 rows
    """
    if type == 2 and w == 21:
        w_ori = w
        w = w - 1
    else:
        w_ori = w


    df['ma20'] = df['close'].rolling(window=w).mean()
    # 표준편차
    df['stddev'] = df['close'].rolling(window=w).std()
    df['upper'] = df['ma20'] + (df['stddev'] * 2)
    df['lower'] = df['ma20'] - (df['stddev'] * 2)
    df['pb'] = (df['close'] - df['lower']) / (df['upper'] - df['lower'])

    # mean() 함수는 '평균을 구하기 위한' numpy 패키지에 포함되어 있는 내장 함수입니다.
    # 20일 이평선이자 볼린저밴드 중앙선
    # mbb = df['close'].rolling(window=w).mean()
    # 종가
    # close = df['close'][:w]

    if type == 1:
        # 중심 가격 Typical Price
        df['tp'] = (df['high'] + df['low'] + df['close']) / 3
        df['pmf'] = 0
        df['nmf'] = 0

        for i in range(len(df.close) - 1):
            if df.tp.values[i] < df.tp.values[i+1]:
                df.pmf.values[i+1] = df.tp.values[i+1] * df.volume.values[i+1]
                df.nmf.values[i+1] = 0
            else:
                df.nmf.values[i+1] = df.tp.values[i+1] * df.volume.values[i+1]
                df.pmf.values[i+1] = 0

        df['mfr'] = df.pmf.rolling(window=10).sum() / df.nmf.rolling(window=10).sum()
        df['mfi10'] = 100 - 100 / (1 + df['mfr'])

    elif type == 2:
        df['ii'] = (2 * df['close'] - df['high'] - df['low']) / (df['high'] - df['low']) * df['volume']
        # logger.debug(df['ii'].rolling(window=w).sum())
        df['iip21'] = (df['ii'].rolling(window=w_ori).sum() / df['volume'].rolling(window=w_ori).sum()) * 100
        # logger.debug(df['iip21'])

    '''
        std (표준편차 값)과 mbb(중앙선)을 이용하여 볼린저밴드
        1. ubb (상한선)
        2. lbb (하한선)
        3. perb (%b: 볼린저밴드에서의 종가 위치)
        4. bw (밴드폭)
        4개의 값을 구하는 수식을 01)볼린저밴드 개념에 안내되어 있는 [볼린저 밴드 계산방법]을 참고하여 구현해주세요.
        변수 이름은 ubb, lbb, perb, bw로 통일하여 주시기 바랍니다.
        
        mbb = 중심선 = 주가의 20 기간 이동평균선 = clo20
        ubb = 상한선 = 중심선 + 주가의 20기간 표준편차 * 2
        lbb = 하한선 = 중심선 – 주가의 20기간 표준편차 * 2
        perb = %b = (주가 – 하한선) / (상한선 – 하한선) = (close - lbb) / (ubb - lbb)
        bw = 밴드폭 (Bandwidth) = (상한선 – 하한선) / 중심선 = (ubb - lbb) / mbb
        mfr = 현금 흐름 비율 Money Flow Ratio = 기간(며칠) 동안의 긍정적 현금 흐름의 합을 기간 동안의 부정적 현금 흐름의 합으로 나눈 결과
        mfi = 현금흐름지표, Money Flow Index = 100 - (100 / (1 + 긍정적 현금 흐름 / 부정적 현금 흐름))
    '''

    ### blank ###########################
    # ubb = mbb + (std * 2)
    # lbb = mbb - (std * 2)
    #####################################

    # if ubb > lbb:
    #     ### blank ###########################
    #     perb = (close - lbb) / (ubb - lbb)
    #     bw = (ubb - lbb) / mbb
    #     #####################################
    #     return mbb, ubb, lbb, perb, bw, df['mfi10']
    # else:
    #     return False

    if type == 1:
        return df[(w - 1):]
    elif type == 2:
        # df = df.dropna()
        # logger.debug(df)
        return df


def ThreeScreens(df):

    ema60 = df.close.ewm(span=60).mean()
    ema130 = df.close.ewm(span=130).mean()
    macd = ema60 - ema130
    signal = macd.ewm(span=45).mean()
    macdhist = macd - signal
    df = df.assign(ema130=ema130, ema60=ema60, macd=macd, signal=signal, macdhist=macdhist).dropna()

    ndays_high = df.high.rolling(window=14, min_periods=1).max()
    ndays_low = df.low.rolling(window=14, min_periods=1).min()

    fast_k = (df.close - ndays_low) / (ndays_high - ndays_low) * 100
    slow_d = fast_k.rolling(window=3).mean()
    df = df.assign(fast_k=fast_k, slow_d=slow_d).dropna()

    return df


class DualMomentum:
    def __init__(self, date_rows_yesterday, invest_unit, engine_daily_buy_list, engine_daily_craw, day_before, date_rows, get_now_close_price_by_date):
        # sql = f"""
        #            SELECT *
        #            FROM `{date_rows_yesterday}` a
        #            WHERE NOT exists(SELECT null FROM stock_konex b WHERE a.code = b.code)
        #                AND  volume != 0
        #                AND close < '{invest_unit}'
        #                ORDER BY volume * close DESC limit 100
        #        """
        # self.realtime_daily_buy_list_temp = engine_daily_buy_list.execute(sql).fetchall()
        # logger.debug('INIT dual momentum')
        self.date_rows_yesterday = date_rows_yesterday
        self.invest_unit = invest_unit
        self.engine_daily_buy_list = engine_daily_buy_list
        self.day_before = day_before
        self.date_rows = date_rows
        self.get_now_close_price_by_date = get_now_close_price_by_date
        self.engine_daily_craw = engine_daily_craw

    def get_rltv_momentum(self, start_date, end_date, stock_count, i):
        """ 특정기간 동안 수익률이 제일 높았던 stock count 개의 종목들 (상대 모멘텀)
        """

        rows = []

        if i < self.day_before + 1:
            pass
        else:
            columns = ['code', 'code_name', 'returns']

            sql = "SELECT * FROM `" + start_date + "` a " \
                    "WHERE NOT exists (SELECT null FROM stock_konex b WHERE a.code=b.code) " \
                    "AND close < '%s' "
            realtime_daily_buy_list_temp = self.engine_daily_buy_list.execute(sql % (self.invest_unit))
            for item in realtime_daily_buy_list_temp:
                code = item.code
                code_name = item.code_name
                yes_close = item.close
                date_before = self.date_rows[i - 1 - self.day_before][0]
                date_before_close = self.get_now_close_price_by_date(code, date_before)
                if date_before_close != 0 and date_before_close != False:
                    # 모멘텀 계산 : n일전 종가 대비 수익률
                    diff_point_calc = (yes_close - date_before_close) / date_before_close * 100
                    # 모멘텀(수익률)이 self.diff_point 보다 높을 경우 realtime_daily_buy_list에 append

                    returns = (yes_close - date_before_close) / date_before_close * 100
                    rows.append([code, code_name, returns])

                    df = pd.DataFrame(rows, columns=columns)
                    df = df[['code', 'code_name', 'returns']]
                    df = df.sort_values(by='returns', ascending=False)
                    df = df.head(stock_count)
                    df.index = pd.Index(range(stock_count))

                    logger.debug(df)
                    logger.debug(f"\nRelative momentum ({start_date} ~ {end_date}) : "\
                                 f"{df['returns'].mean():.2f}% \n")

                return df
